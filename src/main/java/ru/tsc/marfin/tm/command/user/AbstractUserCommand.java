package ru.tsc.marfin.tm.command.user;

import ru.tsc.marfin.tm.api.service.IUserService;
import ru.tsc.marfin.tm.command.AbstractCommand;
import ru.tsc.marfin.tm.enumerated.Role;
import ru.tsc.marfin.tm.exception.entity.UserNotFoundException;
import ru.tsc.marfin.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    public IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    protected void showUser(final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

}
