package ru.tsc.marfin.tm.command.task;

import ru.tsc.marfin.tm.api.service.ITaskService;
import ru.tsc.marfin.tm.command.AbstractCommand;
import ru.tsc.marfin.tm.enumerated.Status;
import ru.tsc.marfin.tm.model.Task;
import ru.tsc.marfin.tm.util.DateUtil;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    public ITaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

    protected void renderTasks(final List<Task> tasks) {
        int index = 1;
        for (final Task task: tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task);
            index++;
        }
    }

    protected void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(task.getCreated()));
        System.out.println("START DATE: " + DateUtil.toString(task.getDateStart()));
        System.out.println("END DATE: " + DateUtil.toString(task.getDateEnd()));
    }

}
