package ru.tsc.marfin.tm.service;

import ru.tsc.marfin.tm.api.service.IAuthService;
import ru.tsc.marfin.tm.api.service.IUserService;
import ru.tsc.marfin.tm.exception.entity.UserNotFoundException;
import ru.tsc.marfin.tm.exception.field.LoginEmptyException;
import ru.tsc.marfin.tm.exception.field.PasswordEmptyException;
import ru.tsc.marfin.tm.exception.field.PasswordIncorrectException;
import ru.tsc.marfin.tm.exception.system.AccessDeniedException;
import ru.tsc.marfin.tm.model.User;
import ru.tsc.marfin.tm.util.HashUtil;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userService.findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        if (!user.getPasswordHash().equals(HashUtil.salt(password))) throw new PasswordIncorrectException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public User registry(final String login, final String password, final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public User getUser() {
        final String userId = getUserId();
        return userService.findOneById(userId);
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

}
