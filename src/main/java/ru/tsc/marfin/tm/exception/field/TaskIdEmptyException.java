package ru.tsc.marfin.tm.exception.field;

public final class TaskIdEmptyException extends AbstractFieldException{

    public TaskIdEmptyException() {
        super("Error! Task Id is empty...");
    }

}

